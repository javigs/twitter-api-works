#!/usr/bin/python3
# -*- coding: utf-8 -*-

import twitter
import datetime
import time
import os

LIMIT_DAYS = 6 * 30
TW_USER = os.environ['TW_MY_ACCOUNT']
TW_CONSUMER_KEY=os.environ['TW_CONSUMER_KEY']
TW_CONSUMER_SECRET=os.environ['TW_CONSUMER_SECRET']
TW_ACCESS_TOKEN_KEY=os.environ['TW_ACCESS_TOKEN_KEY']
TW_ACCESS_TOKEN_SECRET=os.environ['TW_ACCESS_TOKEN_SECRET']

def getApi():
    api = None
    try:
        api = twitter.Api(consumer_key=TW_CONSUMER_KEY,
                          consumer_secret=TW_CONSUMER_SECRET,
                          access_token_key=TW_ACCESS_TOKEN_KEY,
                          access_token_secret=TW_ACCESS_TOKEN_SECRET)
    except twitter.TwitterError as err:
        print("Exception: %s\n" % err.message)

    return api

def getTimeline(api, max_id, how_many=50):
    return api.GetUserTimeline(screen_name=TW_USER, trim_user=True, count=how_many, max_id=max_id)

def getLatest(api):
    iterate = True;
    timeline = getTimeline(api, 0)

    while (iterate):
        print('Iterating %d' % len(timeline))
        next = getTimeline(api, timeline[-1].id)
        if (len(next) > 1):
            timeline = next
        else:
            iterate = False

    return timeline

def destroy(api, tweet_id):
    try:
        api.DestroyStatus(tweet_id)
        time.sleep(0.5)
    except twitter.TwitterError as err:
        print("Exception: %s\n" % err.message)

def main():
    api = getApi()
    if (api == None):
        print("Can't access twitter API")
        exit(1)

    days_ago = datetime.date.today() - datetime.timedelta(days=LIMIT_DAYS)
    print("Destroy items before $s", str(days_ago))
    timeline = getLatest(api)
    for tweet in timeline:
        date_tweet = datetime.datetime.strptime(tweet.created_at,'%a %b %d %H:%M:%S +0000 %Y').date()
        if (date_tweet < days_ago):
            print("Destroying %d [%s]: %s" % (tweet.id, str(date_tweet), tweet.text))
            destroy(api, tweet.id)

if __name__ == "__main__":
    main()
