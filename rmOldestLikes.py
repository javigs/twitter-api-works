#!/usr/bin/python3
# -*- coding: utf-8 -*-

import twitter
import datetime
import time
import os

LIMIT_DAYS = 10 * 30
TW_USER = os.environ['TW_MY_ACCOUNT']
TW_CONSUMER_KEY=os.environ['TW_CONSUMER_KEY']
TW_CONSUMER_SECRET=os.environ['TW_CONSUMER_SECRET']
TW_ACCESS_TOKEN_KEY=os.environ['TW_ACCESS_TOKEN_KEY']
TW_ACCESS_TOKEN_SECRET=os.environ['TW_ACCESS_TOKEN_SECRET']

def getApi():
    api = None
    try:
        api = twitter.Api(consumer_key=TW_CONSUMER_KEY,
                          consumer_secret=TW_CONSUMER_SECRET,
                          access_token_key=TW_ACCESS_TOKEN_KEY,
                          access_token_secret=TW_ACCESS_TOKEN_SECRET)
    except twitter.TwitterError as err:
        print("Exception: %s\n" % err.message)

    return api

def getLikes(api, max_id, how_many=50):
    return api.GetFavorites(screen_name=TW_USER, count=how_many, max_id=max_id)

def getLatest(api):
    iterate = True;
    items = getLikes(api, 0)

    while (iterate):
        print('Iterating %d' % len(items))
        next = getLikes(api, items[-1].id)
        if (len(next) > 1):
            items = next
        else:
            iterate = False

    return items

def destroy(api, id):
    try:
        api.DestroyFavorite(status_id=id)
        time.sleep(0.5)
    except twitter.TwitterError as err:
        print("Exception: %s\n" % err.message)

def main():
    api = getApi()
    if (api == None):
        print("Can't access twitter API")
        exit(1)

    days_ago = datetime.date.today() - datetime.timedelta(days=10*30)
    print("Destroy items before $s", str(days_ago))
    favorites = getLatest(api)
    for like in favorites:
        date_like = datetime.datetime.strptime(like.created_at,'%a %b %d %H:%M:%S +0000 %Y').date()
        if (date_like < days_ago):
            print("Destroying %d [%s]: %s" % (like.id, str(date_like), like.text))
            destroy(api, like.id)

if __name__ == "__main__":
    main()
